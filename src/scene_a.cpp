#include "scene_a.h"

SceneA::SceneA(int w, int h)
{
    Initialize(w,h);
}

void SceneA::Initialize(int w, int h) {
     _zScale          = 1.0;
     _color           = false;
     _invert          = 0.0;
     _play            = false;
     _zSpeed          = 1.0;
     _planeScale      = 0.05;
     _width = w;
     _height = h;
}

void SceneA::Resize(int w, int h) {
    _width = w;
    _height = h;
    glViewport (0, 0, (GLsizei)_width, (GLsizei)_height);
}

int SceneA::LoadTGA(char* fileName) {
    Tga *tga = new Tga();
    
    tgaInfo *info;
    int mode,aux;
    float pointHeight;
    
    // load the image, using the tgalib
	info = tga->TgaLoad(fileName);
	
	// check to see if the image was properly loaded
	// remember: only greyscale, RGB or RGBA noncompressed images
	if (info->status != TGA_OK)
		return(info->status);

	// if the image is RGB, convert it to greyscale.
	// mode will store the image's number of components
	mode = info->pixelDepth / 8;
	if (mode == 3) {
		tga->TgaRGBtogreyscale(info);
		mode = 1;
	}
	
	// set the width and height of the terrain
	_matrixWidth = info->width;
	_matrixHeight = info->height;
	
	printf("image size: %d %d\n",_matrixWidth,_matrixHeight);

    for (int y = 0 ; y < _matrixHeight; y++) {
        for (int x = 0;x < _matrixWidth; x++) {
            // compute the height as a value between 0.0 and 1.0
            aux = mode*(y*_matrixWidth + x);
            pointHeight = info->imageData[aux+(mode-1)] / 256.0;
            _matrix[y*_matrixWidth+x] = pointHeight;
        }
    }
    
    tga->TgaDestroy(info);
    return 0;
}

void SceneA::Animate() {
     if (_play) _zSpeed+=0.03;

     if (_zSpeed>100.0) {
         _zSpeed=1.0;
     }
}

void SceneA::Reset() {
    _zSpeed=1.0;
}

void SceneA::Draw() {
    glPushMatrix();
    glTranslatef(-_planeScale*_matrixWidth/2,-_planeScale*_matrixHeight/2,0.0);

    glDisable (GL_LIGHTING);

    for (int x=0;x<_matrixWidth;x++) {
        glBegin(GL_LINE_STRIP);
        for (int y=0;y<_matrixHeight;y++) {
            colorize(_matrix[y*_matrixWidth+x]*_zScale);
            drawPoint(x*0.05,y*0.05,_matrix[y*_matrixWidth+x]*_zScale,_planeScale);
        }
        glEnd();
    }

    for (int y=0;y<_matrixHeight;y++) {
        glBegin(GL_LINE_STRIP);
        for (int x=0;x<_matrixWidth;x++) {
            colorize(_matrix[y*_matrixWidth+x]*_zScale);
            drawPoint(x*0.05,y*0.05,_matrix[y*_matrixWidth+x]*_zScale,_planeScale);
        }
        glEnd();
    }
    glPopMatrix();
}

void SceneA::colorize(float z) {
    if (_color) {
        if (_invert) glColor3f(1.0-z,0.0,z);
        else glColor3f(z,0.0,1.0-z);
    } else {
        if (_invert) glColor3f(_invert-z,_invert-z,_invert-z);
        else glColor3f(z,z,z);
    }
}

void SceneA::drawPoint(float x, float y, float z, float side) {
     z *= _zSpeed;
     glVertex3f(x,y,z);
}

void SceneA::ToggleColor() {
    _color = !_color; 
}

void SceneA::InvertColor() {
    (_invert==0.0)?_invert=1.0:_invert=0.0; 
}

void SceneA::ToggleAnimation() {
     _play = !_play;
}

void SceneA::set3dScreen(float xCam, float yCam, float zCam) {
    //impostazione prospettiva
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat)_width/(GLfloat)_height, 0.1, 500.0);

    //impostazione camera
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt (xCam, 0.0, zCam, xCam, 0.0, zCam-5, 0.0, 1.0, 0.0);

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}
