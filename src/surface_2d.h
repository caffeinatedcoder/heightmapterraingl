#ifndef SURFACE2D_H
#define SURFACE2D_H

#include <QString>
#include <vector>

#include <GL/glut.h>

using namespace std;

typedef struct {
    unsigned id;
    QString str;
    int x;
    int y;
    float r;
    float g;
    float b;
} Sticky;

class Surface2d
{
private:
    unsigned _maxLines;
    vector<QString> *_lines;
    vector<Sticky> *_stickies;
    int _width,_height;
    void drawString(const char *str, float x, float y);
public:
    Surface2d(int w, int h);
    void Initialize(int w, int h);
    void Resize(int w, int h);
    void Display();

    void AddLine(QString line);
    void AddLine(const char *format, int value);
    void AddLine(const char *format, float value);

    void SetSticky(Sticky s);
    void SetSticky(unsigned id, QString str, int x, int y, float r, float g, float b);
    void UpdateSticky(unsigned id, QString str);
    void UpdateSticky(unsigned id, const char *format, int value);
    void UpdateSticky(unsigned id, const char *format, float value);
};

#endif // SURFACE2D_H
