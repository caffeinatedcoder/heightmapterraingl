#ifndef TEXTIO_H
#define TEXTIO_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

class TextIO
{
private:
    char *_filename;
    vector<string> *_lines;
    void readAll();
public:
    TextIO(char *filename);
};

#endif // TEXTIO_H
