#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "tga.h"

class SceneB {
      private:
              int       _matrixWidth, _matrixHeight;
              float     _matrix[1024*1024];
              float     _zScale;
              float     _planeScale;
              bool      _color;
              float     _invert;
              
              bool      _play;
              float     _zSpeed;
              
              void colorize(float z);
              void drawPoint(float x, float y, float z, float side);
              void normalize(float vector[3]);
              void calcQuadNormal(float v[3][3], float out[3]);
      public:
             SceneB();
             void Init();
             void Draw();      
             void Animate();
             void ToggleColor();
             void InvertColor();
             void ToggleAnimation();
             int LoadTGA(char* fileName);
             
};
