#ifndef OGLUTILS_H
#define OGLUTILS_H

#include <vector>
#include <QString>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

using namespace std;

/*vector<unsigned int> *vboIndices = new vector<unsigned int>();

void ogl_generate_buffers() {
    GLuint elementbuffer;
    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &vboIndices[0], GL_STATIC_DRAW);
}*/

inline void ogl_init() {
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);

    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);

    glutSetCursor(GLUT_CURSOR_NONE);
}

inline void ogl_start_2d_scene(int width, int height) {
    //2D
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho (0, width, height, 0, 0, 1);
    glMatrixMode (GL_MODELVIEW);

    //2D STANDARD FEATURES
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glAlphaFunc(GL_GREATER,0.1f);
    glEnable(GL_BLEND);
    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);
    glDisable(GL_TEXTURE_2D);
}

inline void ogl_end_2d_scene() {
    glMatrixMode (GL_PROJECTION);
    glPopMatrix();
}


inline void ogl_start_3d_scene(int width, int height) {
    //3D SETUP
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat) width/(GLfloat) height, 0.1, 500.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //3D STANDARD FEATURES
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_BLEND);

    glLoadIdentity();

    glPushMatrix();
}

inline void ogl_end_3d_scene() {
    glPopMatrix();
}

inline void ogl_clear() {
    //CLEAR
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}

inline QString ogl_info_version() {
    char buf[100];
    sprintf(buf, "VERSION: %s", glGetString(GL_VERSION));
    return buf;
}

inline QString ogl_info_vendor() {
    char buf[100];
    sprintf(buf, "VENDOR: %s", glGetString(GL_VENDOR));
    return buf;
}

inline QString ogl_info_renderer() {
    char buf[100];
    sprintf(buf, "RENDERER: %s", glGetString(GL_RENDERER));
    return buf;
}

inline QString ogl_info_sl_version() {
    char buf[100];
    sprintf(buf, "SL V: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
    return buf;
}

inline void ogl_status() {
  printf("GL Status: ");

  GLenum status = (GLenum)glGetError();
  switch(status) {
    case GL_NO_ERROR:
      printf("OK\n");
    case GL_INVALID_ENUM:
      printf("GL_INVALID_ENUM\n");
      break;
    case GL_INVALID_VALUE:
      printf("GL_INVALID_VALUE\n");
      break;
    case GL_INVALID_OPERATION:
      printf("GL_INVALID_OPERATION\n");
      break;
    case GL_STACK_OVERFLOW:
      printf("GL_STACK_OVERFLOW\n");
      break;
    case GL_STACK_UNDERFLOW:
      printf("GL_STACK_UNDERFLOW\n");
      break;
    case GL_OUT_OF_MEMORY:
      printf("GL_OUT_OF_MEMORY\n");
      break;
    case GL_INVALID_FRAMEBUFFER_OPERATION_EXT:
      printf("GL_INVALID_FRAMEBUFFER_OPERATION_EXT\n");
      break;
    default:
      printf("UNKNWON\n");
  }
}

#endif // OGLUTILS_H


