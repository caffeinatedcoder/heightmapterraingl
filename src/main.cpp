#ifndef __MAIN__
#define __MAIN__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "utils_ogl.h"
#include "text_io.h"
#include "surface_2d.h"
#include "scene_a.h"
#include "scene_b.h"
#include "vbo_test.h"

#define TERRAIN_ERROR_INVALID_PARAM		-5
#define TERRAIN_ERROR_LOADING_IMAGE		-4
#define TERRAIN_ERROR_MEMORY_PROBLEM	-3
#define	TERRAIN_ERROR_NOT_SAVED			-2
#define TERRAIN_ERROR_NOT_INITIALISED	-1
#define TERRAIN_OK						 0

static int framerate = 30;
static int fullscreen = 0;
static int width = 0;
static int height = 0;

static float zScale = 1.0;
static float planeScale = 0.05;

static int zcam = 5, xcam = 0, spin = 0;
static float explode = 1, efx = 1;
static int renderingMode = 0;
static int appPause = 1, grow = 0, color = 1, invert = 0;
static int specular = 1;
static int diffuse = 1;

SceneA *sceneA = new SceneA(854,480);
SceneB *sceneB = new SceneB();
Surface2d *console = new Surface2d(854,480);

VBOTest *test = new VBOTest();

void gameLoop() {
    if (!appPause)
       spin = (spin + 1) % 360;
    
    if (grow) {
        explode+=0.03;
        efx = explode;
    } else {
        explode = 1;
        efx = explode;     
    }
    
    sceneA->Animate();
    sceneB->Animate();

    //printf("%d %d %d\n",xcam,zcam,spin);

    console->UpdateSticky(1,"zcam %1",zcam);
    console->UpdateSticky(2,"spin %1",spin);

}

void display(void)
{
    ogl_clear();

    ogl_start_3d_scene(width,height);

    gluLookAt (xcam, 0.0, zcam, xcam, 0.0, zcam-5, 0.0, 1.0, 0.0);

    //glRotatef(spin,0.0,0.0,1.0);
    //glRotatef(spin,0.0,1.0,0.0);
    //glRotatef(-spin,1.0,0.0,0.0);

    //sceneA->Draw();

    test->Draw();

    ogl_end_3d_scene();

    //ogl_start_2d_scene(width,height);

    //console->Display();

    //ogl_end_2d_scene();

	glutSwapBuffers();
}

void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
	width = w;
	height = h;

    sceneA->Resize(w,h);
    console->Resize(w,h);
}

void keyboard (unsigned char key, int x, int y)
{
	switch (key) {
		case 'w':
            zcam-=1;
			//glutPostRedisplay();
			break;
		case 's':
            zcam+=1;
			break;
		case 'd':
			xcam++;
			//glutPostRedisplay();
		break;
		case 'a':
			xcam--;
			break;
		case 27: //esc
			exit(0);
			break;
		case '1': renderingMode = 0; break;
		case '2': renderingMode = 1; break;
		case '3': renderingMode = 2; break;
		case '4': renderingMode = 3; break;
		case '5': renderingMode = 4; break;
		case '6': renderingMode = 5; break;
		case '7': renderingMode = 6; break;
        case 'p': (appPause==0)?appPause=1:appPause=0; break;
		case 'o': 
             sceneA->ToggleAnimation(); 
             //sceneB->ToggleAnimation();
             break;
        case 'l':
             sceneA->Reset();
             //sceneB->ToggleAnimation();
             break;
		case 'c': 
             sceneA->ToggleColor(); 
             sceneB->ToggleColor(); 
             break;
		case 'z': (diffuse==0)?diffuse=1:diffuse=0; break;
		case 'x': (specular==0)?specular=1:specular=0; break;
		case 'i': 
             (invert==0)?invert=1:invert=0; 
             glClearColor (invert, invert, invert, invert);
             sceneA->InvertColor();
             //sceneB->InvertColor();
             break;
        case 'b': 
             sceneA->LoadTGA("test1.tga");
             //sceneB->LoadTGA("test1.tga");
             break;
        case 'n': 
             sceneA->LoadTGA("test2.tga");
             //sceneB->LoadTGA("test2.tga");
             break;
        case 'm': 
             sceneA->LoadTGA("test3.tga");
             //sceneB->LoadTGA("test3.tga");
             break;
        //case 'h': info->Toggle();
        default:
			break;
	}

    //console->AddLine("key pressed");
}

void idleFunc(){
	static int lastUpdate = 0;
	static int frames = 0;
	
	int currentTime = glutGet( GLUT_ELAPSED_TIME );
	frames++;
	
	if ( ( currentTime - lastUpdate ) >= 1000/framerate ){
		char buf[20];
		sprintf( buf, "FPS: %i", frames/1000 );
        console->UpdateSticky(0,buf);
		glutSetWindowTitle( buf );
		frames = 0;
		lastUpdate = currentTime;
		gameLoop();
		glutPostRedisplay();
	}
}


int main(int argc, char** argv)
{
    int w = 854;
    int h = 480;

    /*printf("Use keyboard:\n");
    printf("1,2,3,4,5,6,7: 	change rendering mode\n");
    printf("a,s,d,w: 	move the viewpoint\n");
    printf("i:		invert background\n");
    printf("o:		change heightmap\n");
    printf("p:		pause rotation\n");
    printf("z:		toggle diffuse and ambient materials\n");
    printf("x:		toggle specular material\n");
    printf("c:		change color (on rendering mode 1 & 3)\n");
    printf("b,n,m:		change image\n");
    printf("esc:		quit application\n\n\n");

    printf("contacts: daniele.pagliero@gmail.com\n\n\n");*/
    
    sceneA->LoadTGA("gfx.tga");
    console->SetSticky(0,"framerate",w-100,12,1,1,0);
    console->SetSticky(1,"a",w-100,24,0,1,0);
    console->SetSticky(2,"b",w-100,36,0,1,0);
    console->SetSticky(3,"---",w-100,36,0,1,0);

	glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
	if (fullscreen==1) {
		glutGameModeString( "1024x768:32@75" ); 
		glutEnterGameMode();
	} else {
        glutInitWindowSize (w, h);
		glutInitWindowPosition (100, 100);
		glutCreateWindow (argv[0]);
	}

    //ogl_init();

    console->AddLine(ogl_info_version());
    console->AddLine(ogl_info_vendor());
    console->AddLine(ogl_info_renderer());
    console->AddLine(ogl_info_sl_version());

	glutDisplayFunc(display); 
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idleFunc);
	
	glutMainLoop();

    
    char str [0];
    scanf ("%s",str);
    printf("exit");
    return 0;
}

#endif
