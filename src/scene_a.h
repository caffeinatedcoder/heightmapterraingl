#ifndef SCENE_A_H
#define SCENE_A_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "tga.h"

class SceneA {
      private:
              int _width,_height;
              int       _matrixWidth, _matrixHeight;
              float     _matrix[1024*1024];
              float     _zScale;
              float     _planeScale;
              bool      _color;
              float     _invert;
              
              bool      _play;
              float     _zSpeed;
              
              void colorize(float z);
              void drawPoint(float x, float y, float z, float side);
              void set3dScreen(float xCam, float yCam, float zCam);
      public:
             SceneA(int w, int h);
             void Initialize(int w, int h);
             void Resize(int w, int h);
             void Draw();      
             void Animate();
             void Reset();
             void ToggleColor();
             void InvertColor();
             void ToggleAnimation();
             int LoadTGA(char* fileName);
             
};

#endif
