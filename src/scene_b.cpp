#include "scene_b.h"

SceneB::SceneB() {

}

void SceneB::Init() {
     _zScale          = 1.0;
     _color           = false;
     _invert          = 0.0;
     _play            = false;
     _zSpeed          = 1.0;
     _planeScale      = 0.05;
}

int SceneB::LoadTGA(char* fileName) {
    Tga *tga = new Tga();
    
    tgaInfo *info;
    int mode,aux;
    float pointHeight;
    
    // load the image, using the tgalib
	info = tga->TgaLoad(fileName);
	
	// check to see if the image was properly loaded
	// remember: only greyscale, RGB or RGBA noncompressed images
	if (info->status != TGA_OK)
		return(info->status);

	// if the image is RGB, convert it to greyscale.
	// mode will store the image's number of components
	mode = info->pixelDepth / 8;
	if (mode == 3) {
		tga->TgaRGBtogreyscale(info);
		mode = 1;
	}
	
	// set the width and height of the terrain
	_matrixWidth = info->width;
	_matrixHeight = info->height;
	
	printf("image size: %d %d\n",_matrixWidth,_matrixHeight);

    for (int y = 0 ; y < _matrixHeight; y++) {
        for (int x = 0;x < _matrixWidth; x++) {
            // compute the height as a value between 0.0 and 1.0
            aux = mode*(y*_matrixWidth + x);
            pointHeight = info->imageData[aux+(mode-1)] / 256.0;
            _matrix[y*_matrixWidth+x] = pointHeight;
        }
    }
    
    tga->TgaDestroy(info);
    return 0;
}

void SceneB::Animate() {
    if (_play) _zSpeed+=0.03;

    if (_zSpeed>100.0) {
        _zSpeed=1.0;
    }
}

void SceneB::Draw() {
     
     glPushMatrix();
     
     glTranslatef(-_planeScale*_matrixWidth/2,-_planeScale*_matrixHeight/2,0.0);
     
    GLfloat mat_amb_diff[] = { 0.0, 0.4, 1.0, 1.0 };
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_amb_diff);
    
    GLfloat mat_specular[] = { 0.5, 0.5, 0.5, 1.0 };
    GLfloat low_shininess[] = { 5.0 };
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, low_shininess);
              
     for (int x=0;x<_matrixWidth-1;x++) {
        glBegin(GL_QUADS);
        for (int y=0;y<_matrixHeight-1;y++) {
            
            float vx[4][3];
            
            vx[0][0] = x*_planeScale;
            vx[0][1] = y*_planeScale;
            vx[0][2] = _matrix[y*_matrixWidth+x]*_zScale*_zSpeed;
            
            vx[1][0] = (x+1)*_planeScale;
            vx[1][1] = y*_planeScale;
            vx[1][2] = _matrix[y*_matrixWidth+(x+1)]*_zScale*_zSpeed;
            
            vx[2][0] = (x+1)*_planeScale;
            vx[2][1] = (y+1)*_planeScale;
            vx[2][2] = _matrix[(y+1)*_matrixWidth+(x+1)]*_zScale*_zSpeed;
            
            vx[3][0] = x*_planeScale;
            vx[3][1] = (y+1)*_planeScale;
            vx[3][2] = _matrix[(y+1)*_matrixWidth+x]*_zScale*_zSpeed;
            float r[3];
            calcQuadNormal(vx,r);
            
            glNormal3f(r[0],r[1],r[2]);
            
            glVertex3f(vx[0][0],vx[0][1],vx[0][2]);
            glVertex3f(vx[1][0],vx[1][1],vx[1][2]);
            glVertex3f(vx[2][0],vx[2][1],vx[2][2]);
            glVertex3f(vx[3][0],vx[3][1],vx[3][2]);
        }
        glEnd();
    }
    
    glPopMatrix();
}

void SceneB::colorize(float z) {
    if (_color) {
        if (_invert) glColor3f(1.0-z,0.0,z);
        else glColor3f(z,0.0,1.0-z);
    } else {
        if (_invert) glColor3f(_invert-z,_invert-z,_invert-z);
        else glColor3f(z,z,z);
    }
}

void SceneB::drawPoint(float x, float y, float z, float side) {
     z *= _zSpeed;
     glVertex3f(x,y,z);
}

void SceneB::normalize(float vector[3]) {
	float length;							
	length = (float)sqrt((vector[0]*vector[0]) + (vector[1]*vector[1]) + (vector[2]*vector[2]));

	if(length == 0.0f)						
		length = 1.0f;						

	vector[0] /= length;						
	vector[1] /= length;						
	vector[2] /= length;
}

void SceneB::calcQuadNormal(float v[3][3], float out[3])
{
	float v1[3],v2[3];
	static const int x = 0;	
	static const int y = 1;
	static const int z = 2;	

	// Finds The Vector Between 2 Points By Subtracting
	// The x,y,z Coordinates From One Point To Another.

	// Calculate The Vector From Point 1 To Point 0
	v1[x] = v[0][x] - v[1][x];					// Vector 1.x=Vertex[0].x-Vertex[1].x
	v1[y] = v[0][y] - v[1][y];					// Vector 1.y=Vertex[0].y-Vertex[1].y
	v1[z] = v[0][z] - v[1][z];					// Vector 1.z=Vertex[0].y-Vertex[1].z
	// Calculate The Vector From Point 2 To Point 1
	v2[x] = v[1][x] - v[2][x];					// Vector 2.x=Vertex[0].x-Vertex[1].x
	v2[y] = v[1][y] - v[2][y];					// Vector 2.y=Vertex[0].y-Vertex[1].y
	v2[z] = v[1][z] - v[2][z];					// Vector 2.z=Vertex[0].z-Vertex[1].z
	// Compute The Cross Product To Give Us A Surface Normal
	out[x] = v1[y]*v2[z] - v1[z]*v2[y];				// Cross Product For Y - Z
	out[y] = v1[z]*v2[x] - v1[x]*v2[z];				// Cross Product For X - Z
	out[z] = v1[x]*v2[y] - v1[y]*v2[x];				// Cross Product For X - Y

	normalize(out);						// Normalize The Vectors
}

void SceneB::ToggleColor() {
    _color = !_color; 
}

void SceneB::InvertColor() {
    (_invert==0.0)?_invert=1.0:_invert=0.0; 
}

void SceneB::ToggleAnimation() {
     _play = !_play;     
}
