#ifndef VBO_TEST_H
#define VBO_TEST_H

#define GL_GLEXT_PROTOTYPES

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <stdio.h>

#include "utils_ogl.h"


static const int BufferSize = 2;
static GLuint BufferName[BufferSize];

static const GLsizei VertexCount = 6;

enum
{
    POSITION_OBJECT = 0,
    COLOR_OBJECT = 1
};

static const GLsizeiptr PositionSize = 6 * 2 * sizeof(GLfloat);
static const GLfloat PositionData[] =
{
    -1.0f,-1.0f,
     1.0f,-1.0f,
     1.0f, 1.0f,
     1.0f, 1.0f,
    -1.0f, 1.0f,
    -1.0f,-1.0f,
};

static const GLsizeiptr ColorSize = 6 * 3 * sizeof(GLubyte);
static const GLubyte ColorData[] =
{
    255,   0,   0,
    255, 255,   0,
      0, 255,   0,
      0, 255,   0,
      0,   0, 255,
    255,   0,   0
};

class VBOTest
{
public:
    VBOTest();
    void Draw();
};

#endif // VBO_TEST_H
