#include "surface_2d.h"

Surface2d::Surface2d(int w, int h)
{
    Initialize(w,h);
}

void Surface2d::Initialize(int w, int h) {
    _width = w;
    _height = h;

    _maxLines = 20;
    _lines = new vector<QString>();
    _stickies = new vector<Sticky>();
}

void Surface2d::Resize(int w, int h) {
    _width = w;
    _height = h;
    glViewport(0,0,_width,_height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0,_width,_height,0.0);
}

void Surface2d::Display() {
    glColor3f(0.0,1.0,0.0);
    unsigned first = (_lines->size()<_maxLines)?0:_lines->size()-_maxLines;
    for (unsigned i=first; i < _lines->size(); i++) {
        drawString(_lines->at(i).toStdString().c_str(),12,13*(i-first)+12);
    }

    for (unsigned i=0; i < _stickies->size(); i++) {
        Sticky s = _stickies->at(i);
        glColor3f(s.r,s.g,s.b);
        drawString(s.str.toStdString().c_str(),s.x,s.y);
    }
}

void Surface2d::AddLine(QString line) {
    _lines->push_back(line);
}

void Surface2d::AddLine(const char *format, int value) {
    AddLine(QString(format).arg(value));
}

void Surface2d::AddLine(const char *format, float value) {
    AddLine(QString(format).arg(value));
}

void Surface2d::SetSticky(unsigned id, QString str, int x, int y, float r, float g, float b) {
    Sticky s;
    s.id = id;
    s.str = str;
    s.x = x;
    s.y = y;
    s.r = r;
    s.g = g;
    s.b = b;
    _stickies->push_back(s);
}

void Surface2d::SetSticky(Sticky s) {
    _stickies->push_back(s);
}

void Surface2d::UpdateSticky(unsigned id, QString str) {
    for (unsigned i=0; i < _stickies->size(); i++) {
        if (_stickies->at(i).id==id) {
            _stickies->at(i).str = str;
        }
    }
}

void Surface2d::UpdateSticky(unsigned id, const char *format, int value) {
    UpdateSticky(id,QString(format).arg(value));
}

void Surface2d::UpdateSticky(unsigned id, const char *format, float value) {
    UpdateSticky(id,QString(format).arg(value));
}

void Surface2d::drawString(const char *str, float x, float y) {
    glPushMatrix();
    for (unsigned i=0;i<strlen(str);i++) {
        glRasterPos2f(x+(i*10.0),10.0+y);
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13,str[i]);
    }
    glPopMatrix();
}


