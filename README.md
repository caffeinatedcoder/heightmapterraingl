# README #

## INIT THE REPO

1. Create a command-line project with QT named HeightMapTerrainGL
2. Setup Git in the project folder

```
git init
git remote add origin git@bitbucket.org:caffeinatedcoder/heightmapterraingl.git
```

## LINUX

using GLUT on linux/ubuntu is ridicoluosly easy, just install the library with:

```
sudo apt-get install freeglut3 freeglut3-dev
```

then on the .pro file of the QT project just add the linker directives:

```
QT += opengl
LIBS += -lglut -lGLU
INCLUDEPATH += -L/usr/lib/
```