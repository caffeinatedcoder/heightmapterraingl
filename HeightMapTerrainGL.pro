#-------------------------------------------------
#
# Project created by QtCreator 2015-04-20T11:02:37
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = HeightMapTerrainGL
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

#OPENGL LINKER

#linux/ubuntu:
#sudo apt-get install freeglut3 freeglut3-dev

LIBS += -lGLU -lGL -lglut
#INCLUDEPATH += -L/usr/lib/nvidia-331
#INCLUDEPATH += -L/usr/lib/


SOURCES += \
    src/main.cpp \
    src/scene_a.cpp \
    src/scene_b.cpp \
    src/surface_2d.cpp \
    src/text_io.cpp \
    src/tga.cpp \
    src/vbo_test.cpp

OTHER_FILES += \
    res/_font_set.psd \
    res/gfx.tga \
    res/test1.tga \
    res/test2.tga \
    res/test3.tga \
    res/test4.tga \
    README.md

HEADERS += \
    src/utils_ogl.h \
    src/scene_a.h \
    src/scene_b.h \
    src/surface_2d.h \
    src/text_io.h \
    src/tga.h \
    src/utils_drawing.h \
    src/vbo_test.h

#TODO: moving extra files from res to build path
#copyfiles.commands = cp res/ -r $${DESTDIR}
#QMAKE_EXTRA_TARGETS += copyfiles
#POST_TARGETDEPS += copyfiles
